<?php

namespace App\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Http\FormRequest;

class Register extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        return [
            'name' => 'required|string|max:128',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function validInput()
    {
        return [
            'username' => $this->username,
            'name' => $this->name,
            'email' => $this->email,
            'password' => Hash::make($this->password),
            'phone' => $this->email,
            'address' => $this->address,
            'city' => $this->city,
            'country' => $this->country,
            'postcode' => $this->postcode,
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
       return [];
    }
}
