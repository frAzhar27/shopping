<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Http\Request\Register;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    /**
     * Register new user from mobile app.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Register $request)
    {
        $user = User::create($request->validInput());

        $credentials = $request->only('email', 'password');

        if (! $token = auth('api')->attempt($credentials)) {
            return $this->sendFailResponse();
        }

        return response()->json([
            'email' => $user->email,
            'token' => $token,
            'username' => $user->username,
        ]);;
    }
}
