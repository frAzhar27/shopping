<?php

use App\Models\Shopping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\ShoppingController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('signup', RegisterController::class)->name('api.signup');
Route::post('signin', [AuthController::class, 'login'])->name('api.sigin');
Route::get('users', [UserController::class, 'showAll'])->name('api.get.users');
Route::post('shopping', [ShoppingController::class, 'create'])->name('api.create.shopping');
Route::get('shopping', [ShoppingController::class, 'index'])->name('api.get.shopping');
